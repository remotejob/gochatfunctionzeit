package handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func Chats(w http.ResponseWriter, r *http.Request) {

	req, err := http.NewRequest("GET", "http://165.227.101.53:7000/v0/mldata/chats", nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json,text/plain, */*")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {

		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	defer resp.Body.Close()

	results := make(map[string]int64)

	body, err := ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &results)

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(results)

}
